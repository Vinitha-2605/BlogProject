from django.shortcuts import render
from .forms import BlogForm, BlogPostForm, CommentsForm
from django.http import HttpResponse
from .models import Blog,BlogPost, Comments
from django.db.models import Q

# Create your views here.

def createblog(request):
    if (request.method == "POST"):
        form = BlogForm(request.POST, request.FILES) 
        
        if form.is_valid():
           form.save()
           return HttpResponse("Submitted Successfully")
    else:
     form = BlogForm()
    return render(request, "AddBlog.html",{
        "forms": form
    })

def viewblogpost(request):
    post = BlogPost.objects.all()
    return render(request, "BlogPost.html",{
       "blogpost": post
    })

def viewblog(request):
    blog = Blog.objects.all()
    return render(request, "BlogView.html",{
       "blogs": blog
    })

def createpost(request):
    if (request.method == "POST"):
        form = BlogPostForm(request.POST, request.FILES) 
        
        if form.is_valid():
           form.save()
           return HttpResponse("Submitted Successfully")
    else:
     form = BlogPostForm()
    return render(request, "AddBlogPost.html",{
        "forms": form
    })

def editpost(request, id):
    if (request.method == "POST"):
       form = BlogPostForm(request.POST, request.FILES)

       if form.is_valid():
        blogdetails = BlogPost.objects.get(id=id)
        blogdetails.Title = form.cleaned_data['Title']
        blogdetails.Content = form.cleaned_data['Content']
        blogdetails.Image = form.cleaned_data['Image']
        blogdetails.save()
        return HttpResponse("Saved Suucessfully")
    else:
     post = BlogPost.objects.get(id=id)
     form = BlogPostForm()
     form.initial['Title'] = post.Title
     form.initial['Content'] = post.Content
     form.initial['Image'] = post.Image

     return render(request, "AddBlogPost.html", {
       "forms": form
    })  

def delepost(request, id):
   blogdetails = BlogPost.objects.get(id=id)
   blogdetails.delete()
   return HttpResponse("Deleted Successfully")

def commentpost(request, id):
   if request.method == "POST":
      form = CommentsForm(request.POST)
      post = BlogPost.objects.get(id=id)
       
      if form.is_valid():
         comments = Comments(Comments = form.cleaned_data['Comments'],
                             post = post,
                             createdby = request.session['User'])
         comments.save()
         return HttpResponse("Saved Successfully")
   else:
    comments = CommentsForm()
    commentsdetails = Comments.objects.filter(post=id)
    print(commentsdetails)
    return render(request, "PostComments.html", {
      "forms": comments,
      "comments":commentsdetails
   })

def searchpost(request):
   breakpoint()
   Title =request.GET
   Content = request.GET

   post = BlogPost.objects.filter(Q(Title__contains=Title['Title']) |
                                  Q(Content__contains=Content['Content']))
   
#    post = BlogPost.objects.filter(Q(Title=Title['Title'])|
#                                   Q(Content=Content['Content']))
   
   return render(request, "BlogPost.html",{
      "blogpost":post
   })