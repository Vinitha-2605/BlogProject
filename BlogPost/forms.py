from django import forms
from .models import Blog, BlogPost, Comments

class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ['Title', 'Description', 'Images']

class BlogPostForm(forms.ModelForm):
    class Meta:
        model = BlogPost
        fields = ['Title', 'Content', 'Image', 'Blog']

class CommentsForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['Comments']