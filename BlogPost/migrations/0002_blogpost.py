# Generated by Django 4.2 on 2023-05-09 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BlogPost', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Title', models.CharField(max_length=20)),
                ('Content', models.TextField()),
                ('Image', models.ImageField(upload_to='photos')),
                ('createdby', models.CharField(max_length=25)),
            ],
        ),
    ]
