# Generated by Django 4.2 on 2023-05-10 10:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BlogPost', '0006_comments'),
    ]

    operations = [
        migrations.AddField(
            model_name='comments',
            name='createdby',
            field=models.CharField(max_length=25, null=True),
        ),
    ]
