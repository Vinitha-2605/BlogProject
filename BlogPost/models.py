from django.db import models

# Create your models here.

class Blog(models.Model):
    Title = models.CharField(max_length=60)
    Description = models.CharField(max_length=50)
    Images = models.ImageField(upload_to='photos')
    def __str__(self):
        return f"{self.Title}"

class BlogPost(models.Model):
    Title = models.CharField(max_length=60)
    Content = models.TextField()
    Image = models.ImageField(upload_to='photos')
    Blog = models.ForeignKey(Blog, on_delete=models.CASCADE, null=True)
    createdby = models.CharField(max_length=30)
    def __str__(self):
        return f"{self.id},{self.Title},{self.Content},{self.Image},{self.createdby}"
    
class Comments(models.Model):
    Comments = models.TextField(max_length=80)
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE)
    createdby = models.CharField(max_length=25, null=True)
    def __str__(self):
        return f"{self.Comments},{self.createdby}"