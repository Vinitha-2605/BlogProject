from django.urls import path
from . import views

urlpatterns = [
    path('blog', views.viewblog),
    path('blogpost', views.viewblogpost),
    path('addblog', views.createblog),
    path('addpost', views.createpost),
    path('editpost/<id>', views.editpost),
    path('deletepost/<id>', views.delepost),
    path('commentpost/<id>', views.commentpost, name ="comments"),
    path('searchpost', views.searchpost, name="search")
]